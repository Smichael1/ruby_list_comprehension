Gem::Specification.new do |s|
  s.name        = 'ruby_list_comprehension'
  s.version     = '0.2.4'
  s.date        = '2019-10-02'
  s.summary     = 'Ruby List Comprehension'
  s.description = 'List Comprehensions for Ruby'
  s.authors     = ['Samuel Michael']
  s.email       = 'smichael@appacademy.io'
  s.files       = ['lib/ruby_list_comprehension.rb']
  s.homepage    =
    'https://github.com/SammoMichael/Ruby_List_Comprehension'
  s.license       = 'MIT'
  s.metadata = {
      "bug_tracker_uri" => "https://github.com/SammoMichael/Ruby_List_Comprehension/issues",
      "changelog_uri" => "https://github.com/SammoMichael/Ruby_List_Comprehension/commits/master",
      "documentation_uri" => "https://github.com/SammoMichael/Ruby_List_Comprehension/blob/master/README.md",
      "homepage_uri" => "https://github.com/SammoMichael/Ruby_List_Comprehension",
      "mailing_list_uri" => "https://groups.example.com/bestgemever",
      "source_code_uri" => "https://github.com/SammoMichael/Ruby_List_Comprehension/blob/master/lib/ruby_list_comprehension.rb",
      "wiki_uri" => "https://github.com/SammoMichael/Ruby_List_Comprehension/wiki"
  }
end